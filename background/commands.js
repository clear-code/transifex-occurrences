/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  configs
} from '/common/common.js';
import * as ProjectDialog from '/common/project-dialog.js';

export async function tryOpenOccurrences(tab) {
  const project = findProjectInfo(tab) || await tryAddProject(tab);
  if (!project) {
    console.log('No project found');
    return;
  }

  const occurrences = await collectOccurrences(tab, project.occurrencesMatcher);
  console.log('Occurrences: ', occurrences);
  if (occurrences.length == 0)
    return;

  const pathExtractor = new RegExp(project.pathExtractor || configs.defaultPathExtractor);
  const lineExtractor = new RegExp(project.lineExtractor || configs.defaultLineExtractor);
  let isFirst = true;
  let index = tab.index + 1;
  for (const occurrence of occurrences) {
    const pathMatched = occurrence.match(pathExtractor);
    const lineMatched = occurrence.match(lineExtractor);
    const path = pathMatched && (pathMatched[1] || pathMatched[0]);
    const line = lineMatched && (lineMatched[1] || pathMatched[0]) || 1;
    if (!path)
      throw new Error(`No path found from ${JSON.stringify(occurrence)} by the matcher ${JSON.stringify(pathExtractor.source)}`);
    const url = project.sourceUrl.replace(/%PATH%/gi, path).replace(/%LINE%/gi, line);
    browser.tabs.create({
      openerTabId: tab.id,
      active: isFirst,
      url,
      index
    });
    index++;
    isFirst = false;
  }
}

function findProjectInfo(tab) {
  const allProjects = configs.projects.concat(configs.builtinProjects);
  for (const project of allProjects) {
    const urlPattern = new RegExp(`^${escapeRegExpMetaCharacters(project.urlPattern).replace(/\\\?/g, '.').replace(/\\\*/g, '.*')}`);
    if (urlPattern.test(tab.url))
      return project;
  }
  return null;
}

function escapeRegExpMetaCharacters(string) {
  return string.replace(/[-[\]{}()*+?\.,\\^$|#\s]/g, '\\$&');
}

async function collectOccurrences(tab, matcher = null) {
  if (!matcher)
    matcher = configs.defaultOccurrencesMatcher;
  const occurrences = await browser.tabs.executeScript(tab.id, {
    code: `(() => {
      const occurrencesHolders = document.evaluate(
        '/descendant::node()[text()=${JSON.stringify(configs.occurrencesLabel)}]/following-sibling::*[1]',
        document,
        null,
        XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
        null
      );
      let foundOccurrences = [];
      const matcher = new RegExp(${JSON.stringify(matcher)}, 'g');
      for (let i = 0, maxi = occurrencesHolders.snapshotLength; i < maxi; i++) {
        const occurrencesHolder = occurrencesHolders.snapshotItem(i);
        const occurrences = occurrencesHolder.textContent.match(matcher);
        if (!occurrences || occurrences.length == 0)
          continue;
        foundOccurrences = foundOccurrences.concat(Array.from(occurrences));
      }
      return foundOccurrences;
    })();`
  });
  return occurrences.flat();
}

async function tryAddProject(tab) {
  const project = await ProjectDialog.showInTab(tab);
  if (!project)
    return null;

  const projects = JSON.parse(JSON.stringify(configs.projects));
  let updated = false;
  for (let i = 0, maxi = project.length; i < maxi; i++) {
    const oldProject = projects[i];
    if (oldProject.urlPattern != project.urlPattern)
      continue;
    projects.splice(i, 1, project);
    updated = true;
  }
  if (!updated)
    projects.push(project);
  configs.projects = projects;
  return project;
}
