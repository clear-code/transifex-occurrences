/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Commands from './commands.js';

browser.menus.create({
  id:                  'openOccurrences',
  title:               browser.i18n.getMessage('menu_openOccurrences_title'),
  contexts:            ['editable', 'page', 'selection'],
  documentUrlPatterns: ['https://www.transifex.com/*']
});

browser.menus.onClicked.addListener((info, tab) => {
  switch (info.menuItemId) {
    case 'openOccurrences':
      return Commands.tryOpenOccurrences(tab);
  }
});
