/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import Options from '/extlib/Options.js';
import '/extlib/l10n.js';

import {
  configs
} from '/common/common.js';
import * as ProjectDialog from '/common/project-dialog.js';

const options = new Options(configs);

function onConfigChanged(key) {
  switch (key) {
    case 'showBuiltinProjects':
      if (configs.showBuiltinProjects)
        document.documentElement.classList.add('show-builtin-projects');
      else
        document.documentElement.classList.remove('show-builtin-projects');
      break;

    case 'debug':
      if (configs.debug)
        document.documentElement.classList.add('debugging');
      else
        document.documentElement.classList.remove('debugging');
      break;
  }
}

let gProjects;
let gBuiltinProjects;

function getElementTarget(node) {
  if (node.nodeType == Node.TEXT_NODE)
    return node.parentNode;
  return node;
}

function addProjectRow(project, isBuiltin = false) {
  const row = document.createElement('tr');
  row.project = project;
  row.classList.add('project');
  if (isBuiltin)
    row.classList.add('builtin');
  const urlPatternCol = row.appendChild(document.createElement('td'));
  const link = urlPatternCol.appendChild(document.createElement('a'));
  link.href = '#';
  link.textContent = project.urlPattern;
  const controlCol = row.appendChild(document.createElement('td'));
  controlCol.classList.add('control');
  if (isBuiltin) {
    gBuiltinProjects.appendChild(row);
  }
  else {
    const deleteButton = controlCol.appendChild(document.createElement('button'));
    deleteButton.classList.add('delete');
    deleteButton.textContent = browser.i18n.getMessage('config_deleteProject_label');
    gProjects.appendChild(row);
  }
  return row;
}

async function addNewProject() {
  const project = await ProjectDialog.show({
    onShown: (container) => {
      const dialog = container.closest('.rich-confirm-dialog');
      const rect = document.getElementById('projectRows').getBoundingClientRect();
      dialog.style.top = `calc(${rect.top}px + 1em)`;
    }
  });
  if (!project)
    return;

  addProjectRow(project);
  saveProjects();
}

async function editProject(row) {
  const project = await ProjectDialog.show({
    project: row.project,
    onShown: (container) => {
      const dialog = container.closest('.rich-confirm-dialog');
      const rect = row.getBoundingClientRect();
      dialog.style.top = `${rect.top}px`;
    }
  });
  if (!project)
    return;

  if (row.classList.contains('builtin')) {
    row = addProjectRow(project);
    saveProjects();
  }

  row.project = project;
  if (project.urlPattern.trim() == '') // invalid pattern!
    return editProject(row);

  row.firstChild.firstChild.textContent = project.urlPattern;
  saveProjects();
}

function deleteProject(row) {
  row.parentNode.removeChild(row);
  saveProjects();
}

function saveProjects() {
  const projects = [];
  for (const row of document.querySelectorAll('#projectRows .project')) {
    projects.push(JSON.parse(JSON.stringify(row.project)));
  }
  configs.projects = projects;
}

function addCommandListener(element, listener) {
  element.addEventListener('click', event => {
    if (event.button != 0)
      return;
    listener(event);
  });
  element.addEventListener('keydown', event => {
    if (/^(Enter|Space| )$/.test(event.key))
      return;
    listener(event);
  });
}

configs.$addObserver(onConfigChanged);
window.addEventListener('DOMContentLoaded', () => {
  if (/^Mac/i.test(navigator.platform))
    document.documentElement.classList.add('platform-mac');
  else
    document.documentElement.classList.remove('platform-mac');

  configs.$loaded.then(() => {
    gProjects = document.getElementById('projectRows');
    gBuiltinProjects = document.getElementById('builtinProjectRows');
    addCommandListener(gProjects, event => {
      const target = getElementTarget(event.target);
      const row = target.closest('.project');
      if (!row)
        return;
      if (target.closest(':link'))
        editProject(row);
      else if (target.closest('.delete'))
        deleteProject(row);
    });
    addCommandListener(gBuiltinProjects, event => {
      const target = getElementTarget(event.target);
      const row = target.closest('.project');
      if (!row)
        return;
      if (target.closest(':link'))
        editProject(row);
    });
    addCommandListener(document.getElementById('addNewProject'), addNewProject);
    for (const project of configs.projects) {
      addProjectRow(project);
    }
    for (const project of configs.builtinProjects) {
      addProjectRow(project, true);
    }

    onConfigChanged('showBuiltinProjects');
    options.buildUIForAllConfigs(document.querySelector('#group-allConfigs'));
    onConfigChanged('debug');
  });
}, { once: true });
