/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import Configs from '/extlib/Configs.js';

import * as OwnCloud from './projects/owncloud.js';

export const configs = new Configs({
  occurrencesLabel: browser.i18n.getMessage('occurrencesLabel'),

  defaultOccurrencesMatcher: /([^\s,]+(?::[0-9]+)?)/.source,
  defaultPathExtractor:      /^(.+):/.source,
  defaultLineExtractor:      /:(\d+)$/.source,
  defaultSourceUrl:          browser.i18n.getMessage('defaultSourceUrl'),

  projects: [],
  builtinProjects: [
    // SomeProject.projects,
    // AnotherProject.projects,
    // ...
    OwnCloud.projects
  ].flat(),
  showBuiltinProjects: false,

  debug: false
}, {
  localKeys: `
    showBuiltinProjects
    debug
  `.trim().split('\n').map(aKey => aKey.trim()).filter(aKey => aKey && aKey.indexOf('//') != 0)
});
