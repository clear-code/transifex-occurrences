/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  configs
} from '/common/common.js';
import RichConfirm from '/extlib/RichConfirm.js';

export async function show(options = {}) {
  return showInTab(null, options);
}

export async function showInTab(tab, options = {}) {
  const project = options.project || {};

  // ex. https://www.transifex.com/organization/project/translate/#language_name/subproject/00000
  const urlPattern         = project.urlPattern || (tab && tab.url.replace(/(https:\/\/www.transifex.com\/.+\/translate\/#)[^\/]+(\/[^\/]+\/).*/, '$1*$2*')) || '';
  const occurrencesMatcher = project.occurrencesMatcher || configs.defaultOccurrencesMatcher;
  const pathExtractor      = project.pathExtractor || configs.defaultPathExtractor;
  const lineExtractor      = project.lineExtractor || configs.defaultLineExtractor;
  const sourceUrl          = project.sourceUrl || configs.defaultSourceUrl;
  const fieldStyle         = `
    -webkit-appearance: textfield;
    width:              100%;
  `;
  const dialogParams = {
    content: `
      <div><label>${HTMLSafeMessage('dialog_urlPattern_label')}<br>
                  <input type="text"
                         name="urlPattern"
                         required
                         style="${fieldStyle}"
                         value="${HTMLSafe(urlPattern)}"></label></div>
      <div><label>${HTMLSafeMessage('dialog_occurrencesMatcher_label')}<br>
                  <input type="text"
                         name="occurrencesMatcher"
                         style="${fieldStyle}"
                         value="${HTMLSafe(occurrencesMatcher)}"></label></div>
      <div><label>${HTMLSafeMessage('dialog_pathExtractor_label')}<br>
                  <input type="text"
                         name="pathExtractor"
                         style="${fieldStyle}"
                         value="${HTMLSafe(pathExtractor)}"></label></div>
      <div><label>${HTMLSafeMessage('dialog_lineExtractor_label')}<br>
                  <input type="text"
                         name="lineExtractor"
                         style="${fieldStyle}"
                         value="${HTMLSafe(lineExtractor)}"></label></div>
      <div><label>${HTMLSafeMessage('dialog_sourceUrl_label')}<br>
                  <input type="text"
                         name="sourceUrl"
                         style="${fieldStyle}"
                         value="${HTMLSafe(sourceUrl)}"></label></div>
    `,
    onShown: options.onShown,
    buttons: [
      browser.i18n.getMessage('dialog_accept'),
      browser.i18n.getMessage('dialog_cancel')
    ]
  };

  const result = tab ? await RichConfirm.showInTab(tab.id, dialogParams) : await RichConfirm.show(dialogParams);

  if (result.buttonIndex != 0)
    return null;

  return {
    urlPattern:         result.values.urlPattern,
    occurrencesMatcher: result.values.occurrencesMatcher,
    pathExtractor:      result.values.pathExtractor,
    lineExtractor:      result.values.lineExtractor,
    sourceUrl:          result.values.sourceUrl
  };
}

function HTMLSafe(string) {
  return string
    .replace(/&/g, '&amp;')
    .replace(/"/g, '&quot;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
}

function HTMLSafeMessage(key) {
  return HTMLSafe(browser.i18n.getMessage(key));
}
