/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

const DEFAULT_OCCURRENCES_MATCHER = /([^\s,]+(?::[0-9]+)?)/.source;
const DEFAULT_PATH_MATCHER = /^(.+):/.source;
const DEFAULT_LINE_MATCHER = /:(\d+)$/.source;

function ownCloudTopLevelProject(name, options = {}) {
  return {
    urlPattern:         `https://www.transifex.com/owncloud-org/owncloud/translate/#*/${name}/*`,
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          `https://github.com/${options.owner || 'owncloud'}/${options.repository || name}/blob/master/%PATH%#L%LINE%`
  };
}

function ownCloudTopLevelArchivedProject(name) {
  return {
    urlPattern:         `https://www.transifex.com/owncloud-org/owncloud/translate/#*/${name}/*`,
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          `https://github.com/owncloudarchive/${name}/blob/master/%PATH%#L%LINE%`
  };
}

function ownCloudAppsProject(name) {
  return {
    urlPattern:         `https://www.transifex.com/owncloud-org/owncloud/translate/#*/${name}/*`,
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          `https://github.com/owncloud/apps/tree/master/${name}/%PATH%#L%LINE%`
  };
}

function ownCloudCoreAppsProject(name) {
  return {
    urlPattern:         `https://www.transifex.com/owncloud-org/owncloud/translate/#*/${name}/*`,
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          `https://github.com/owncloud/core/tree/master/apps/${name}/%PATH%#L%LINE%`
  };
}

export const projects = [
  // https://www.transifex.com/owncloud-org/owncloud/dashboard/
  ownCloudTopLevelProject('activity'),
  ownCloudAppsProject('admin_migrate'),
  ownCloudTopLevelProject('announcementcenter'),
  ownCloudTopLevelProject('bookmarks'),
  ownCloudTopLevelProject('brute_force_protection'),
  ownCloudTopLevelProject('calendar'),
  ownCloudTopLevelProject('chat'),
  ownCloudCoreAppsProject('comments'),
  ownCloudTopLevelProject('contacts'),
  {
    urlPattern:         'https://www.transifex.com/owncloud-org/owncloud/translate/#*/core/*',
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          'https://github.com/owncloud/core/tree/master/core/%PATH%#L%LINE%'
  },
  ownCloudTopLevelProject('customgroups'),
  ownCloudCoreAppsProject('dav'),
  ownCloudTopLevelProject('diagnostics'),
  ownCloudTopLevelArchivedProject('documents'),
  ownCloudTopLevelProject('external'),
  ownCloudCoreAppsProject('federatedfilesharing'),
  ownCloudCoreAppsProject('federation'),
  ownCloudCoreAppsProject('files'),
  ownCloudTopLevelProject('files_antivirus'),
  ownCloudTopLevelProject('files_encryption', { repository: 'encryption' }),
  ownCloudCoreAppsProject('files_external'),
  ownCloudTopLevelProject('files_external_dropbox'),
  ownCloudTopLevelProject('files_external_ftp'),
  ownCloudTopLevelProject('files_external_gdrive'),
  ownCloudAppsProject('files_odfviewer'),
  ownCloudTopLevelProject('files_paperhive'),
  ownCloudCoreAppsProject('files_sharing'),
  ownCloudTopLevelProject('files_texteditor'),
  ownCloudCoreAppsProject('files_trashbin'),
  ownCloudCoreAppsProject('files_versions'),
  ownCloudTopLevelProject('firstrunwizard'),
  ownCloudTopLevelProject('gallery'),
  ownCloudTopLevelProject('guests'),
  ownCloudTopLevelProject('impersonate'),
  ownCloudAppsProject('impress'),
  ownCloudAppsProject('imprint'),
  {
    urlPattern:         'https://www.transifex.com/owncloud-org/owncloud/translate/#*/lib/*',
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          'https://github.com/owncloud/core/blob/master/lib/%PATH%#L%LINE%'
  },
  ownCloudTopLevelArchivedProject('mail'),
  ownCloudTopLevelProject('maps'),
  ownCloudTopLevelProject('market'),
  ownCloudTopLevelProject('mobile-ios', { repository: 'ios' }),
  ownCloudTopLevelProject('mobile-ios-app', { repository: 'ios-app' }),
  {
    urlPattern:         'https://www.transifex.com/owncloud-org/owncloud/translate/#*/mobile-ios-app-infoplist/*',
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          'https://github.com/owncloud/ios-app/blob/master/ownCloud/Resources/en.lproj/InfoPlist.strings'
  },
  ownCloudTopLevelProject('mobile-ios-sdk', { repository: 'ios-sdk' }),
  // "mobile-ios-ui" is missing
  ownCloudTopLevelArchivedProject('mozilla_sync'),
  ownCloudTopLevelProject('music'),
  ownCloudTopLevelArchivedProject('news'),
  ownCloudTopLevelProject('notes'),
  ownCloudTopLevelProject('notifications'),
  // "notifications_mail" is missing
  ownCloudTopLevelProject('oauth2'),
  ownCloudTopLevelProject('oc_files_mv', { owner: 'eotryx' }),
  ownCloudTopLevelProject('ocDashboard', { owner: 'seeks' }),
  ownCloudAppsProject('ownpad_lite'),
  ownCloudTopLevelProject('passman', { owner: 'nextcloud' }),
  ownCloudTopLevelProject('password_policy'),
  ownCloudTopLevelProject('passwords', { owner: '8l4ckSh33p' }),
  ownCloudTopLevelProject('registration', { owner: 'pellaeon' }),
  ownCloudTopLevelProject('richdocuments'),
  ownCloudTopLevelArchivedProject('security'),
  {
    urlPattern:         'https://www.transifex.com/owncloud-org/owncloud/translate/#*/settings-1/*',
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          'https://github.com/owncloud/core/blob/master/settings/%PATH%#L%LINE%'
  },
  ownCloudTopLevelProject('shorty', { owner: 'arkascha', repository: 'owncloud-shorty' }),
  {
    urlPattern:         'https://www.transifex.com/owncloud-org/owncloud/translate/#*/shorty_tracking/*',
    occurrencesMatcher: DEFAULT_OCCURRENCES_MATCHER,
    pathExtractor:      DEFAULT_PATH_MATCHER,
    lineExtractor:      DEFAULT_LINE_MATCHER,
    sourceUrl:          'https://github.com/arkascha/owncloud-shorty/tree/master/shorty_tracking/%PATH%#L%LINE%'
  },
  ownCloudCoreAppsProject('systemtags'),
  ownCloudTopLevelProject('tasks'),
  ownCloudTopLevelProject('templateeditor'),
  ownCloudTopLevelProject('twofactor_backup_codes'),
  ownCloudTopLevelProject('twofactor_totp', { owner: 'nextcloud' }),
  ownCloudCoreAppsProject('updatenotification'),
  ownCloudTopLevelProject('updater'),
  ownCloudTopLevelProject('user_ldap'),
  ownCloudTopLevelProject('user_management'),
  ownCloudAppsProject('user_migrate'),
  ownCloudAppsProject('user_openid_provider'),
  ownCloudTopLevelProject('WindowsPhone', { repository: 'windows-phone' })
];
