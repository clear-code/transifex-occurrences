# Transifex Open Occurrences

This is a Firefox addon helping to visit "occurrences" of a translation.

[Transifex](https://www.transifex.com/) is a portal of projects to translate open source software. On the website, each translation can have its own "occurrences" information (location in the source code) in the "context" tab. This addon provides a new context menu to open them in new tabs, so you can easily check how the sentence is used.

## How to use

When you are seeing a translation, right-click (or Control-click on macOS) anywhere on the page. Then an extra command "Open Occurrences of this translation" appears in the bottom of the menu. Clicking on the item will open occurenced location in the corresponding source code in new tabs.

If it is an unknown project, a dialog to register the project will appear. Then please fill the URL pattern to open its corresponding source and save it.

## Attention

This addon cannot open occurrences if the translation project doesn't provide occurrences information.

## Help Needed

There are only few projects registered by default. We need more help to add more builtin project information like [ownCloud](https://gitlab.com/clear-code/transifex-occurrences/blob/master/common/projects/owncloud.js). Please send merge requests to add new project information freely!

### How to add projects information

 1. Add a new module file under `common/projects/`, like: `myProjects.js`
 2. Define and export an array of project information in the module. Each project information will be in the form:
    
    ```javascript
    export const projects = [
      {
        // A match pattern to detect a project. Wildcards "*" (zero or more any
        // characters) and "?" (one any character) are available.
        urlPattern: 'https://www.transifex.com/organization/project/translate/#*/sub-project/*',
        // A source string of a global regular expression to extract occurrences
        // from a string like:
        //   "path/to/file.js:123, path/to/file.php:456"
        // Note that it is used for global matching.
        occurrencesMatcher: /([^\s,]+(?::[0-9]+)?)/.source,
        // A source string of a regular expression to extract path part from an
        // occurrence, like: "path/to/file.js:123". The first "(...)" part will be
        // extracted as the path, in this case it is "path/to/file.js".
        pathExtractor: /^(.+):/.source,
        // A source string of a regular expression to extract line number part from
        // an occurrence, like: "path/to/file.js:123". The first "(...)" part will
        // be extracted as the line number, in this case it is "123".
        lineExtractor: /:(\d+)$/.source,
        // A URL pattern to open the corresponding source location. "%PATH%" and
        // "%LINE%" will be filled with extracted information based on extractors
        // defined above.
        sourceUrl: 'https://github.com/organization/project/blob/master/%PATH%#L%LINE%`
      },
      ...
    ];
    ```
 3. Import the module from `common/common.js` like `import * as MyProject from './projects/myProjects.js';`.
 4. Add the list of projects defined in the imported module to `builtinProjects`, like:
    
    ```javascript
      builtinProjects: [
        // SomeProject.projects,
        // AnotherProject.projects,
        // ...
        OwnCloud.projects,
        MyProject.projects // ADDED!
      ].flat(),
    ```
 5. `git add common/projects/myProjects.js` and commit all changes.
 6. Send a merge request to this repository.

## License

Mozilla Public License 2.0
