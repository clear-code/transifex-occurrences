#  Transifex Open Occurrences（出現場所を開く） 

 翻訳の「出現場所」を閲覧するのを補助するFirefox用アドオンです。 

[Transifex](https://www.transifex.com/)はオープンソースソフトウェアの翻訳プロジェクトのポータルサイトです。このWebサイト上では、各翻訳が「文脈」タブの中に「出現場所」（ソースコード上の出現位置）の情報を持っている場合があります。このアドオンはコンテキストメニューに、出現場所をタブで開くための機能を追加します。これにより、そのセンテンスがどのように使われているかを簡単に確認する事ができます。

## 使い方

翻訳作業の最中に、ページ内の任意の位置で右クリック（macOSではControl-クリック）すると、コンテキストメニューの一番下に「この翻訳の出現場所を開く」というメニューが表示されます。この項目をクリックすると、翻訳対象のフレーズのソースコード上での出現場所が新しいタブで開かれます。

対応するソースコードのリポジトリが未登録の場合には、プロジェクトを登録するためのダイアログが開かれますので、GitHubなどのリポジトリ上で対象のファイルを開くためのURLのパターンを入力して設定を保存して下さい。

## 注意点

その翻訳プロジェクトが出現場所の情報を登録していない場合、このアドオンでは出現場所を開けません。 

## ご協力のお願い

初期状態で登録されているプロジェクトはごくわずかです。便利に使えるようにするためには、[ownCloudの翻訳プロジェクトの情報](https://gitlab.com/clear-code/transifex-occurrences/blob/master/common/projects/owncloud.js)のように組み込みのプロジェクト情報を充実させていく必要があります。新しいプロジェクトの情報を追加するマージリクエストをぜひお送り下さい。

### プロジェクト情報の追加手順

 1. `common/projects/` 以下に `myProjects.js` のような名前でモジュールのファイルを追加します。
 2. モジュール内でプロジェクトの情報の配列を定義し、エクスポートします。ここのプロジェクトは以下のような形式を取ります:
    
    ```javascript
    export const projects = [
      {
        // プロジェクトを認識するためのマッチパターン。ワイルドカードとして「*」
        // （0個以上の任意の文字）および「?」（任意の1文字） が使えます。
        urlPattern: 'https://www.transifex.com/organization/project/translate/#*/sub-project/*',
        // 複数の出現場所を抽出するためのグローバルマッチ用正規表現のソース。
        // マッチ対象の文字列は、例えば以下の要領です:
        //   "path/to/file.js:123, path/to/file.php:456"
        // この正規表現はグローバルマッチにのみ使われる事に注意が必要です。
        occurrencesMatcher: /([^\s,]+(?::[0-9]+)?)/.source,
        // 「path/to/file.js:123」のような形式の個々の出現場所情報からパス部分を
        // 抜き出すための正規表現のソース。最初の「(〜)」部分がパスとして
        // 抜き出され、この例では「path/to/file.js」がそれにあたります。
        pathExtractor: /^(.+):/.source,
        // 「path/to/file.js:123」のような形式の個々の出現場所情報から行番号部分を
        // 抜き出すための正規表現のソース。最初の「(〜)」部分が行番号として
        // 抜き出され、この例では「123」がそれにあたります。
        lineExtractor: /:(\d+)$/.source,
        // 対応するソースコード上の位置を開くためのURLのパターン。「%PATH%」と
        // 「%LINE%」は上記のルールで抽出された内容で置き換わります。
        sourceUrl: 'https://github.com/organization/project/blob/master/%PATH%#L%LINE%`
      },
      ...
    ];
    ```
 3. `import * as MyProject from './projects/myProjects.js';`のようにして、モジュールを`common/common.js`からインポートする。
 4. インポートしたプロジェクト情報の配列を`builtinProjects`の一覧に書き加える。
    
    ```javascript
      builtinProjects: [
        // SomeProject.projects,
        // AnotherProject.projects,
        // ...
        OwnCloud.projects,
        MyProject.projects // ここが追加された箇所!
      ].flat(),
    ```
 5. `git add common/projects/myProjects.js` などとして、すべての変更をコミットする。
 6. このプロジェクトへのマージリクエストを送信する。

## ライセンス

Mozilla Public License 2.0
