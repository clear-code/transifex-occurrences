/*eslint-env commonjs*/
/*eslint quote-props: ['error', "always"] */

module.exports = {
  'root': true,

  'parserOptions': {
    'ecmaVersion': 2018,
  },

  'env': {
    'browser': true,
    'es6': true,
    'webextensions': true,
  },

  'settings': {
    'import/resolver': {
      'babel-module': {
        'root': ['./'],
      }
    }
  },

  'rules': {
    'import/resolver': {
      'babel-module': {
        'root': ['./'],
      }
    }
  }
};
